# GraphQL, CRUD, MongoDB

## Description
A re&#45;purposed version of a project by @soufianecharkaoui98 on the subjects of GraphQL (with apollo-server, per-se), NodeJS and Mongoose for MongoDB found at Medium.

### Processing and/or completion date(s)
  - September 26, 2020; September 27, 2020; September 28, 2020

### How to run it&hellip;
If you insist on using npm utilize "npm run [directive]" instead of "yarn run [directive]". This is under the presumption that you already have nodejs, mongodb, and yarn installed:

  - step 1: yarn run devbuild (installs the packages)
  - step 2: yarn run startservice (for linux machines to start the systemctl services)
  - step 3: yarn run servermongo (starts the mongodb server)
  - step 4: yarn run dummydata (optional mongodb client)
  - step 5: yarn run start OR yarn run devstart (for use with either nodejs or nodemon, respectively)
  - step 6: yarn run stopservice (for linux machines to stop the systemctl services)
  - step 7: yarn run culldata (frees up 3 GB. of space)

## Attribution
I queried **&quot;graphql sqlite crud example&quot;** at **duck duck go**. One of the results that came up with the following article by @soufianecharkaoui98 called [A Simple CRUD App Using GraphQL, NodeJS, and MongoDB](https://medium.com/better-programming/a-simple-crud-app-using-graphql-nodejs-mongodb-78319908f563 "link to medium") which can be found at medium. The search results were off the mark but the subject of GraphQL is totally relevant to front-end development skills.

## What could be unique about this arrangement
Presently, it is for use with local instance(s) of GraphQL (apollo-server), NodeJS, and MongoDB. You can just as easily swap out the addresses in the **dot env** file for your own purposes.

Also, I wanted to get more acclimated with each async/await, plus, thennable fetch statements in the context of Mongoose for MongoDB. Thus, I composed two different versions of GraphQL resolvers as they apply to MongoDB: one utilizes then/catch statements while the other utilizes async/await statements in order to generate the same results.

One other takeaway is that the response object for the GraphQL queries are usually constituted of the custom object type (**Movie** in this case) as opposed to another native Scalar type (such as **Boolean**). 

In this exercise, we are not dealing with &quot;changed rows&quot;, &quot;affected rows&quot;, etcetera, as you might do so when referring to MySQL, for example. As you may recall, MongoDB is object&#45;based, thus, so are the GraphQL query and mutation responses.

Finally, if you have viewed the other projects that I recently posted immediately previous to this &quot;bad&#45;boy&quot; within the past few weeks, then, you would have noticed that I had formulated some functionality for the GraphQL context. I am just trying to keep it real and forward&#45;ready for any contingency that one day in the not so distant future could require further authorization, authentication or some other custom functionality. You can safely remove the context functionality in this exercise and **nothing will change**. In this instance, context is cosmetic and superficial. 

## Why the fuss over context?!?!?!
I want to close on this subject and sell you once and for all so there is no misunderstanding. 

Allow me to paraphrase from each [Okta Blog](https://developer.okta.com/blog/2019/05/29/build-crud-nodejs-graphql "link to okta blog") and [GitHub](https://github.com/apollographql/apollo-server#context "link to github"), then, quote from [Github](https://github.com/graphql/express-graphql#options "link to github") so that things are more clear to you:

  - &quot;&hellip;in order for resolvers to know whether or not a user is authenticated, the recommended way is to add the user to the context. [...] The context is built before any resolvers are hit.  [...] The context is passed along to each resolver so authentication only needs to happen at the start of any request.&quot;
  - &quot;A request context is available for each request. [...] When context is defined as a function, it will be called on each request and will RECEIVE an object [literal]. [...] The object [literal] will contain a "req" property which represents the request itself. [...] Context will be available as the third positional parameter of the resolvers since the context function returns an object [literal].&quot;
  - &quot;[The context object is] a value to pass as the context to the graphql() function [...] If context is not provided the request object is passed as the context.&quot;

That is all I have to say about that!

### May Jesus bless you
I hope to have influenced you in some positive manner today. 

God bless.