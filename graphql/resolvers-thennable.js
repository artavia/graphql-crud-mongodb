// =============================================
// import Mongoose middleware
// =============================================
const Movie = require("./../custom_models_mongoose/movie.model");

// =============================================
// GraphQL Endpoints
// parent (_), args (__) , context, info  
// =============================================

// THENNABLE STATEMENTS
const resolvers = {

  Query: {
    
    // Queries

    // ...initial test(s)
    hello: async ( _ , __, context ) => {
      // await console.log( "_" , _ );    // RETURNS {}
      // await console.log( "__" , __ );  // RETURNS {}
      
      // await console.log( "context" , await context ); // RETURNS {}

      await // console.log( "context.myProperty" , await context.myProperty ); 
      
      // await console.log( "context.customVersion" , await context.customVersion ); 
      // { "custom-version": "5.1.50.ou812" } 

      return await "Konichiwa, whirlled world!";
    }
    
    // ...the rest
    , movies: async ( _, __ ) => { 
      
      let param = {};

      return await Movie.find( param )
      .then( async ( dvds ) => {
        // await console.log( "dvds", await dvds );
        return await dvds;
      } )
      .catch( (err) => {
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      } );

    } 
    
    , movie: async ( _, __ ) => {
      
      let param = {
        _id: await __.id
      };
      // await console.log( "param" , await param );

      // https://mongoosejs.com/docs/api.html#model_Model.findById

      return await Movie.findOne( await param )
      .then( async ( dvd ) => {
        // await console.log( "dvd", await dvd );
        return await dvd;
      } )
      .catch( (err) => {
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      } );

    }

  }

  , Mutation: {

    // Mutations

    addMovie: async ( _, __ ) => {
      
      let param = {};
      param.name = await __.name;
      param.producer = await __.producer;
      param.rating = await __.rating;
      const movie = new Movie( await param );
      
      return await movie.save()
      .then( async ( newmovie ) => {
        // await console.log( "newmovie", await newmovie );
        return await newmovie;
      } )
      .catch( (err) => {
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      } );
      
    } 

    , updateMovie: async ( _, __ ) => {

      const param = {
        _id: await __.id
      };

      const update = {
        $set: {
          name: await __.name
          , producer: await __.producer
          , rating: await __.rating
        }
      };

      const options = {
        new: await true
        , upsert: await false // default
      };

      return await Movie.findOneAndUpdate( await param, await update, await options )
      .then( async ( updatedmovie ) => {
        // await console.log( "updatedmovie", await updatedmovie );
        return await updatedmovie;
      } )
      .catch( (err) => {
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      } );

    }

    , deleteMovie: async ( _, __ ) => {

      const param = {
        _id: await __.id
      };

      return await Movie.findByIdAndDelete( await param )
      .then( async ( deletedmovie ) => {
        // await console.log( "deletedmovie", await deletedmovie );
        return await deletedmovie;
      } )
      .catch( (err) => {
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      } );

    }

  } 

};

module.exports = { resolvers };