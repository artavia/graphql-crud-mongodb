// =============================================
// import Mongoose middleware
// =============================================
const Movie = require("./../custom_models_mongoose/movie.model");

// =============================================
// GraphQL Endpoints
// parent (_), args (__) , context, info  
// =============================================

// TRY/CATCH STATEMENTS
const resolvers = {

  Query: {
    
    // Queries

    // ...initial test(s)
    hello: async ( _ , __, context ) => {
      
      // await console.log( "_" , _ );    // RETURNS {}
      // await console.log( "__" , __ );  // RETURNS {}
      
      // await console.log( "context" , await context ); // RETURNS {}

      // await console.log( "context.myProperty" , await context.myProperty ); 
      // await console.log( "context.myReqHeaders" , JSON.stringify( await context.myReqHeaders, null, 2 ) );
      // await console.log( "context.customVersion" , await context.customVersion ); 
      // { "custom-version": "5.1.50.ou812" } 

      return await "Konichiwa, whirlled world!";

    }
    
    // ...the rest
    , movies: async ( _, __ ) => {

      try {
        let param = {};
        const dvds = await Movie.find( param ); 
        // await console.log( "dvds", await dvds );
        return await dvds;
      }
      catch(err){
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      }

    }

    , movie: async ( _, __ ) => { 
      
      try {
        
        const param = {
          _id: await __.id
        };
        
        const dvd = await Movie.findOne( await param );
        // await console.log( "dvd", await dvd );
        return await dvd;

      }
      catch(err){
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      }

    } 

  }

  , Mutation: {

    // Mutations

    addMovie: async ( _, __ ) => {
      
      try{
        let param = {};
        param.name = await __.name;
        param.producer = await __.producer;
        param.rating = await __.rating;
        const movie = new Movie( await param );
        let newmovie = await movie.save();
        // await console.log( "newmovie" , await newmovie );
        return await newmovie;
      }
      catch(err){
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      }

    }

    , updateMovie: async ( _, __ ) => {
      
      try {

        const param = {
          _id: await __.id
        };

        const update = {
          $set: {
            name: await __.name
            , producer: await __.producer
            , rating: await __.rating
          }
        };

        const options = {
          new: true
          , upsert: false // default
        };

        const updatedmovie = await Movie.findOneAndUpdate( await param, await update, options );
        // await console.log( "updatedmovie", await updatedmovie );
        return await updatedmovie;

      }
      catch(err){
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      }

    }

    , deleteMovie: async ( _, __ ) => {
      
      try{
        
        const param = {
          _id: await __.id
        };

        const deletedmovie = await Movie.findByIdAndDelete( await param );
        // await console.log( "deletedmovie", await deletedmovie );
        return await deletedmovie;
      }
      catch(err){
        // console.log("YOU HAVE DERRPAGE ~ err: " , err );
        return null;
      }

    } 

  } 

}; 

module.exports = { resolvers };