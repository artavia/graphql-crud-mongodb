const { gql } = require("apollo-server");

const typeDefs = gql`

  # INPUTS
  
  # OBJECT TYPES
  type Movie {
    id: ID
    name: String!
    producer: String!
    rating: Float!
  }
  
  # QUERY TYPES
  type Query {
    
    # ...initial test
    hello: String

    # ... the rest
    movies: [ Movie ]
    movie( id: ID! ): Movie

  }
  
  # MUTATION TYPES
  type Mutation {
    
    addMovie( name: String!, producer: String!, rating: Float! ): Movie
    updateMovie( id: ID!, name: String!, producer: String!, rating: Float! ): Movie
    deleteMovie( id: ID! ): Movie

  }

`;

module.exports = { typeDefs };