db.movies.drop();

db.movies.insert( {
  name: "Raiders of the Lost Ark"
  , rating: 4.75
  , producer: "Steven Spielberg"
  , createdAt: new Date()
} );

db.movies.insert( {
  name: "Star Wars- The Empire Strikes Back"
  , rating: 4.75
  , producer: "George Lucas"
  , createdAt: new Date()
} );

db.movies.insert( {
  name: "Batman Begins"
  , rating: 4.00
  , producer: "Christopher Nolan"
  , createdAt: new Date()
} ); 

