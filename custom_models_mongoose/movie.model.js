const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let MovieSchema = new Schema( {

  name: {
    type: String
    , required: true
  }
  , rating: {
    type: Number
    , required: true
  }
  , producer: {
    type: String
    , required: true
  }
  , createdAt: {
    type: Date
    , required: false
  }
  , updatedAt: {
    type: Date
    , required: false
  }

} 
, {
  timestamps: true
} );

module.exports = mongoose.model( "Movie", MovieSchema );