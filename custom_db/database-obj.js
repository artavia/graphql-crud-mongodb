( () => {
  // =============================================
  // LOCAL DEVELOPMENT ENV VARS SETUP
  // process.env SETUP
  // =============================================
  if( process.env.NODE_ENV !== "production"){
    const dotenv = require("dotenv");
    dotenv.config();
  }
  const { CONNECTION_URL } = process.env;

  // =============================================
  // BASE SETUP
  // =============================================
  const mongoose = require("mongoose");

  // =============================================
  // DB CONNECTION
  // =============================================
  let cachedDb = null;

  const connect = (url) => {
    
    console.log("=> connect to database");
    
    if(cachedDb){
      console.log("=> using cached database instance");
      return Promise.resolve(cachedDb);
    }

    return mongoose.connect( url, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true } )
    .then( (client) => {
      console.log(`MongoDB server running new instance at ${url}`);
      cachedDb = client;
      return cachedDb;
    } )
    .catch( (err) => {
      console.error( "err... uh, oh! " , err );
      throw err;
    } );

  };

  const asyncHandling = async () => {
    let database = await Promise.resolve( connect( CONNECTION_URL ) );
    return database;
  };

  module.exports = asyncHandling;
} )();