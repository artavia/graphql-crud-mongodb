// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { NODE_ENV } = process.env;
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

// =============================================
// IMPORT mongoDB MDUB
// =============================================
const connectDB = require("./custom_db/database-obj");

// =============================================
// graphql boilerplate
// graphql middleware - schemas and resolvers AND context
// IMPLEMENT GRAPHQL ROUTE
// =============================================
const { ApolloServer } = require("apollo-server"); 

const { typeDefs } = require("./graphql/typedefs");

const { resolvers } = require("./graphql/resolvers-try-catch");
// const { resolvers } = require("./graphql/resolvers-thennable");

const { context } = require("./graphql/context");

const server = new ApolloServer( {
  typeDefs: typeDefs
  , resolvers: resolvers
  , context: context
  , playground: IS_PRODUCTION_NODE_ENV ? false : true 
} );

// =============================================
// DB and GraphQL SERVERS INSTANTIATION
// =============================================
connectDB()
.then( async () => {

  await server.listen()
  .then( ( props ) => {
    
    // console.log( "props" , props );
    const { url } = props;
    
    if( IS_PRODUCTION_NODE_ENV === true ){
      console.log( `\napollo-server Production backend is running. \n`);
    }
    if( IS_DEVELOPMENT_NODE_ENV === true ){
      console.log( `\napollo-server Development backend is running. \n`);
      console.log(`\n🚀 The GraphQL playground is ready at ${url} \n`);
    }
    
  } )
  .catch( ( err ) => {
    console.log( "graphql server err" , err );
  } );

} )
.catch( (err) => {
  console.error( "connectDB err", err );
} );
